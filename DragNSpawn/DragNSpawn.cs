﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragNSpawn : MonoBehaviour
{
    [SerializeField]
    LayerMask groundMask;

    //public Transform unitFab;

    public Transform displayPrefab;
    public Transform placePrefab;

    public Camera mainCam;

    public Transform guide;

    Vector3 initCoord;
    Vector3 currCoord;
    Vector3 endCoord;

    private bool dragging;
    public bool addDepth;

    private float sizeOfUnit = 1f;

    private float radius = 0;

    private float numUnitsInLine;

    List<Transform> liveUnits;

    private void Start()
    {
        liveUnits = new List<Transform>();
        addDepth = true;
    }

    // Update is called once per frame
    void Update()
    {
        MouseDragging();
        if (dragging)
        {
            currCoord = ClickedCoord();
            RotateGuide();

            float newRadius = CalculateRadius(initCoord, currCoord);
            if (newRadius != radius)
            {
                radius = newRadius;
                numUnitsInLine = Mathf.Round(radius / sizeOfUnit);

                if (numUnitsInLine == 0)
                {
                    numUnitsInLine = 1;
                }

                StoreUnits((int)numUnitsInLine);
                SpawnUnits((int)numUnitsInLine);
            }


            

        }
    }

    private Vector3 ClickedCoord()
    {
        Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Vector3 clickedCoord = Vector3.zero;

        if (Physics.Raycast(ray, out hit, 100.0f, groundMask))
        {
            clickedCoord = hit.point;   //Coordinates of clicked position.

            
        }

        return clickedCoord;
    }


    private void MouseDragging()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Debug.Log("Beginning to Drag");
            dragging = true;
            initCoord = ClickedCoord();
            guide.transform.localPosition = initCoord;
        }
        if (Input.GetMouseButtonUp(1))
        {
            Debug.Log("Stopped dragging");
            dragging = false;
            endCoord = ClickedCoord();

            PlaceUnits((int)numUnitsInLine);

            
        }
    }

    private float PythagoraTheorem(float x, float y)
    {

        return Mathf.Sqrt((x * x) + (y * y));
    }

    private float CalculateRadius(Vector3 init, Vector3 end)
    {
        float xMagnitude = end.x - init.x;
        float zMagnitude = end.z - init.z;

        float length = PythagoraTheorem(xMagnitude, zMagnitude);

        return length;
    }

    private void SpawnUnits(int numUnits)
    {

        float distanceBetweenEachUnit = 1f;

        for (int i = 0; i < numUnits; i++)
        {
            float offsetForNextUnit = distanceBetweenEachUnit * i;

            Vector3 cosSin = CosSinVector(guide.transform.rotation.eulerAngles.y - 90);

            float xSpawnLoc = initCoord.x + (offsetForNextUnit * cosSin.x);
            float zSpawnLoc = initCoord.z + (offsetForNextUnit * -cosSin.y);

            Vector3 spawnLocation = new Vector3(xSpawnLoc, initCoord.y, zSpawnLoc);
            //Instantiate(unitFab, spawnLocation, unitFab.transform.rotation);

            liveUnits[i].localPosition = spawnLocation;
            liveUnits[i].eulerAngles = new Vector3(0f, guide.localRotation.eulerAngles.y -90, 0f);
            liveUnits[i].gameObject.SetActive(true);



        }

        // Set units outside of radius to be inactive.
        for (int i = 0; i < liveUnits.Count - numUnits; i++)
        {
            if (addDepth)
            {
                Vector3 cosSin = CosSinVector(guide.transform.rotation.eulerAngles.y - 90);

                Transform lead = liveUnits[i];

                Vector3 coordinateBehindLead = new Vector3(cosSin.y, 0f, cosSin.x);

                Transform follower = liveUnits[numUnits + i];
                follower.position = (lead.position - coordinateBehindLead);
                follower.eulerAngles = new Vector3(0f, guide.localRotation.eulerAngles.y - 90, 0f);
                follower.gameObject.SetActive(true);
            } else
            {
                liveUnits[(liveUnits.Count - i) - 1].gameObject.SetActive(false);
            }
            
        }

    }

    private void RotateGuide()
    {
        Ray cameraRay = mainCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        //  Coordinate
        if (Physics.Raycast(cameraRay, out hit, 100.0f, groundMask))
        {

            Vector3 pointToLook = cameraRay.GetPoint(hit.distance);
            // Rotate parent Object
            guide.transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));


        }
    }

    private Vector3 CosSinVector(float angle)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle);
        float y = Mathf.Sin(Mathf.Deg2Rad * angle);

        Vector3 cosSinValues = new Vector3(x, y, 0f);

        return cosSinValues;
    }

    private void StoreUnits(int numUnits)
    {
        int length = liveUnits.Count;

        if (numUnits > length)
        {
            // Calculate how many units to add
            int unitsToAdd = numUnits - length;
            for (int i = 0; i < unitsToAdd; i++)
            {
                GameObject newUnit = Instantiate(displayPrefab.gameObject);
                newUnit.SetActive(false);

                liveUnits.Add(newUnit.transform);
                liveUnits[i].gameObject.SetActive(true);
            }
        }
    }

    void PlaceUnits(int numUnitsInLine)
    {

        int numUnitsToPlace;

        if (addDepth)
        {
            numUnitsToPlace = liveUnits.Count;
        } else
        {
            numUnitsToPlace = numUnitsInLine;
        }

        for (int i = 0; i < numUnitsToPlace; i++)
        {
            Instantiate(placePrefab, liveUnits[i].localPosition, liveUnits[i].localRotation);
        }
    }

}
